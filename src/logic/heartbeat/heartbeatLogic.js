// custom dependencies
const DbHandler = require('../DbHandler');
const Responder = require('./../Responder');
const logger = require('./../../../logger.js');

/**
 * Post a heartbeat to the server whenever a WIFI connection can be established.
 *
 * 1) Check if provided deviceId already exists in the device table
 *
 * 1b) If not, create a new device with the provided Id
 *
 * 2) Store the heartbeat into obd_wifi_log
 *
 * @param {object} req
 * @param {object} res
 */
function postHeartbeat(req, res) {
    logger.verbose('Post Heartbeat functionality triggered');
    const dbHandler = new DbHandler();
    const responder = new Responder();
    const reqDeviceId = req.body.device_id;

    // query condition
    const condition = {
        where: { id: reqDeviceId },
    };

    // 1) check if provided deviceId already exists in the device table
    dbHandler.findOne('device', condition)
        .then(device => new Promise((resolve, reject) => {
            logger.verbose(`Sequelize findOne('device') response: ${device}`);

            // check if a device has been found for the provided Id
            if (!device) {
                // 1b) if not, create a new device with the provided Id
                logger.info(`No device has been found for provided Id: ${reqDeviceId}`);
                logger.info('A new device with the provided deviceId will be created');

                // build a new device with default configuration. In order to force a firmware update as well
                // as a config update asap, the last updated timestamps will have the ISO 8601 value for Januar 1st 1980.
                const newDevice = {
                    id: reqDeviceId,
                    device_type: 'obd',
                    last_firmware_update: '1980-01-01',
                    last_config_update: '1980-01-01',
                };

                // insert new device into device table
                dbHandler.create('device', newDevice)
                    .then((createdDevice) => {
                        logger.info(`A new device has been created with id: ${createdDevice.id}`);
                        resolve();
                    })
                    .catch(error => reject(error));
            } else {
                logger.info('A device has been found for the provided Id');
                resolve();
            }
        }))
        .then(() => new Promise((resolve, reject) => {
            // build new heartbeat entry
            const newHearbeat = req.body;
            logger.verbose('Try posting heartbeat to the database');

            // 2) Store the heartbeat into obd_wifi_log
            dbHandler.create('obd_wifi_log', newHearbeat)
                .then((heartbeat) => {
                    logger.info(`A new heartbeat has been created for device: ${heartbeat.device_id} with time: ${heartbeat.time}`);
                    resolve(heartbeat);
                })
                .catch(error => reject(error));
        }))
        .then(data => responder.send(res, true, '<SuccessfulInsert>', null, data))
        .catch(error => responder.falseAndSend(res, error, 500));
}

exports.postHeartbeat = postHeartbeat;
