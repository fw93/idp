/* eslint-disable import/order */

// make again sure that the environment is 'test'
process.env.NODE_ENV = 'test';

// my app
const app = require('../app');

// testing dependencies
const request = require('supertest')(app);

// custom dependencies
const setupTestDb = require('../src/database/init-db').setupDevDb;

// spec files
const heartbeatsSpecPath = './specs/heartbeats.spec.js';
const recfilesSpecPath = './specs/recfiles.spec.js';
const updatesSpecPath = './specs/updates.spec.js';

describe('API Tests', () => {
    before(() => {
        try {
            // prepare the test database for the upcoming tests
            setupTestDb();
        } catch (error) {
            console.error(`Error while testing: ${error}`);
        }
    });

    /**
     * Testing /api endpoint
     */
    describe('App', () => {
        it('has a default hello world route', (done) => {
            request
                .get('/api')
                .expect(200)
                .expect('Hello World', done);
        });
    });

    // import tests for all end points
    importTest('Heartbeats', heartbeatsSpecPath);
    importTest('Recfiles', recfilesSpecPath);
    importTest('Updates', updatesSpecPath);
});

/**
 * Function to import tests from different files.
 *
 * @param {string} name the name the test should have
 * @param {string} path the path to the test file
 */
function importTest(name, path) {
    describe(name, () => {
        require(path);
    });
}
