// a mock for the form-data input the heartbeats route expects
exports.heartbeatsBodyMock = {
    device_id: '1',
    time: new Date().toUTCString(), // current time
    time_previous_wifi_lost: '2013-05-01 00:00:00',
    time_max_rssi: '2011-01-01 00:00:00',
    ssid_current_wifi: 'asdasdasdhwadjhasdja',
    lat_current_wifi_con: 15.123,
    long_current_wifi_con: 250,
    ssid_previous_wifi: 'daksdjawqdasd123',
    rssi_previous_wifi_lost: 21,
    lat_previous_wifi_lost: 12,
    long_previous_wifi_lost: 14,
    max_rssi: 20,
    lat_max_rssi: 17,
    long_max_rssi: 14,
    free_disk_space: 4444,
    number_of_local_files: 1234,
};
