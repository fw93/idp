// mock to check for updates for device '1'
exports.checkForUpdatesMock = {
    device_id: '1',
};

// valid downloadFirmwareParams input. Downloads a sample pdf (www.africau.edu/images/default/sample.pdf)
exports.downloadFirmwareMock = {
    device_id: '1',
    new_firmware_url: 'www.africau.edu/images/default/sample.pdf',
};


// valid downloadConfigParams input. Downloads a sample pdf (www.africau.edu/images/default/sample.pdf)
exports.downloadConfigMock = {
    device_id: '1',
    new_config_url: 'www.africau.edu/images/default/sample.pdf',
};
