# Installation
If a current version of `Node.js` and `NPM` is installed, all that's left to do is `npm install`
which will create a folder `node_modules` containing all dependencies.

# Configuration
For the API to work correcty, a few additional things have to configurated. These can be found in the `config.js`. 

First, all devices that do not yet exist get newly created and get the default config and firmware. Thus, the 
`defaultFirmwareVersionId` and `urlDefaultConfig` in the `config.js` need to be changed to the appropriate values.

The amount of recfiles that can be uploaded in one request is limited. The limit can be changed in the config file.

Then, the folder where the recfiles are moved to (the recfile queue) might has to be changed to. Note, that it
is advisable, to put the recfile queue folder in the `.gitignore`, if is part of a Git repository. 

# Api Documentation: Swagger
This API is documented via `Swagger`. To see the swagger documentation, run the app and visit `/api-docs`.

# How to run
There are multiple different scripts to run in the `package.json`. For simply testing the app
`npm run dev` is recommended. For running the app while using the productional database, run
`npm run prod`. 

# How to update the Database Models: sequelize-auto
The database is accessed by usage of the ORM Sequelize. All models can be found in `/src/database/models/`.
If the database needs to be adapted, `sequelize-auto` is the tool to update the models via the CLI (https://github.com/sequelize/sequelize-auto).
After altering a table within the PostgreSQL database, the following command can be run in the command line:

`sequelize-auto -h postgres-sm.ftm.mw.tum.de -d mobtrack -u username -x password -p 5432 -e postgres -s device`

The `-h` option describes the hostname. `-d` the name of the database. `u` and `-x` are username and password while `-p` is the port and `e` the dialect.
If only models for certain schemata should be created, a schema to create models for can be specified via `-s`.

After successfully running `sequelize-auto`, files with models for the schema `device` are created. Now the next step is, to only take the models 
of the tables that actually been altered and merge the code the already exisiting models. This can be done easiest by simply using the already 
exisiting model class and just changing the altered columns.

# Tests
All tests can be executed via `npm run test`.
