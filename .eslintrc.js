module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "linebreak-style": ["error", process.env.NODE_ENV === "prod" ? "unix" : "windows"],
        "max-len": [
            "error", {
                "code": 140
            }
        ],
        // import resolving is deactivated since it leads to false negatives with aliases
        // "import/no-unresolved": 0,
        "indent": ["error", 4],
        "allowMultiplePropertiesPerLine": true,
        // "capitalized-comments": [
        //     "error",
        //     "never",
        //     {
        //         "ignoreInlineComments": true,
        //         "ignoreConsecutiveComments": true
        //     }
        // ],
        // "object-curly-newline": ["error", {
        //     "multiline": true,
        //     "minProperties": 4
        // }]
    },
    "env": {
        "jasmine": true,
        "browser": true,
        "node": true,
        "mocha": true
    }
};